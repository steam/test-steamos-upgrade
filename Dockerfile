FROM debian:stretch-slim

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8

RUN \
install -d /etc/apt/sources.list.d && \
echo "deb http://deb.debian.org/debian stretch-backports main" > \
    /etc/apt/sources.list.d/stretch-backports.list && \
apt-get update && \
apt-get -y --no-install-recommends install \
    ca-certificates \
    ovmf \
    qemu-system \
    qemu-utils \
    wget \
&& \
apt-get -t stretch-backports -y --no-install-recommends install \
    autopkgtest \
&& \
:
