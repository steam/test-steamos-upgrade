pipeline {
  agent {
    dockerfile {
      args '--device=/dev/kvm'
    }
  }

  options {
    timestamps()
    skipDefaultCheckout()
  }

  stages {
    stage ("checkout") {
      steps {
          /* Need manual checkout as checkout scm doesn't work with gitlab
           * configuration yet */
          script {
            if (env.gitlabTargetRepoSshURL == null) {
              env.gitlabTargetRepoSshURL = env.gitlabSourceRepoSshURL
            }
          }
          checkout changelog: true, poll: true, scm: [
            $class: 'GitSCM',
            branches: [[name: "source/${env.gitlabSourceBranch}"]],
            extensions: [[$class: 'PreBuildMerge',
                          options: [fastForwardMode: 'FF', mergeRemote: 'target', mergeStrategy: 'default', mergeTarget: "${env.gitlabTargetBranch}"]]],
            userRemoteConfigs: [ [ name: 'target', url: "${env.gitlabTargetRepoSshURL}", credentialsId: 'GITLABSSHKEY' ],
                                 [ name: 'source', url: "${env.gitlabSourceRepoSshURL}", credentialsId: 'GITLABSSHKEY' ]]
          ]
      }
    }

    stage("test") {
      steps {
        sh 'mkdir output'
        sh 'wget -O autopkgtest.qcow2 https://people.collabora.com/~smcv/expires-201802/steamos-2-98.qcow2c'
        sh './run-autopkgtest'
      }
    }
  }

  post {
    always {
      archiveArtifacts artifacts: 'output/**'
      deleteDir()
    }
  }
}
